extends Node2D

@export var mob: PackedScene
@export var count: int = 2
@export var radius: float = 10
@export var spawn_on_start: bool = true
@export_range(0, 10, 0.1) var spawn_between_delay: float = 1.0
@export var player: CharacterBody2D

# Called when the node enters the scene tree for the first time.
func _ready():
	var instance = mob.instantiate()
	instance.add_to_group("agressive_entities")
	instance.player = player
	for _i in count:
		if not spawn_on_start:
			await get_tree().create_timer(spawn_between_delay).timeout
		var i = instance.duplicate()
		i.global_position = global_position + Vector2(randf_range(-radius, radius), randf_range(-radius, radius))
		add_child(i)
