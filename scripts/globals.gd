extends Node

signal relic_finded

var all_relics: int = 0

var relics: int = 0:
	set(new_value):
		if not (new_value == relics + 1):
			return
		relic_finded.emit()
		relics = new_value
