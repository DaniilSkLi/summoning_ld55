extends CharacterBody2D

@export var speed: float = 3.0
@export var avoidance_distance_from_player: float = 30.0
@export var avoidance_distance_from_mobs: float = 3.0
@export var distance_to_attack: float = 35.0
@export var max_player_view_distance: float = 300.0
@export var detect_player_distance: float = 200.0
@export var attack_value: float = 1.0
@export var attack_reload: float = 1.0
@export var random_walk_radius: float = 20.0
@export var random_walk_timeout: float = 2.0

@export var player: CharacterBody2D

@onready var health = $Health
@onready var navigation_agent = $NavigationAgent2D

var _attack_timer: Timer = Timer.new()
var _random_walk_timer: Timer = Timer.new()

var _pursuit: bool = false

func _ready():
	_attack_timer.autostart = false
	_attack_timer.wait_time = attack_reload
	_attack_timer.timeout.connect(_hurt_player)
	add_child(_attack_timer)
	
	_random_walk_timer.autostart = false
	_random_walk_timer.one_shot = true
	_random_walk_timer.wait_time = random_walk_timeout
	_random_walk_timer.timeout.connect(_random_walk_timeout_func)
	navigation_agent.target_position = global_position
	add_child(_random_walk_timer)

func _physics_process(delta):
	var distance_to_player = global_position.distance_to(player.global_position)
	
	if distance_to_player < detect_player_distance:
		_pursuit = true
	elif distance_to_player > max_player_view_distance:
		_pursuit = false
	
	# Set target to player position
	if _pursuit:
		var pos = player.global_position - (player.global_position - global_position).normalized() * avoidance_distance_from_player
		#for mob in get_tree().get_nodes_in_group("agressive_entities"):
			#if mob == self:
				#continue
			#pos -= (mob.global_position - global_position).normalized() * avoidance_distance_from_mobs
		navigation_agent.target_position = pos
	
	# Movement
	if distance_to_player > avoidance_distance_from_player or true:
		var direction = to_local(navigation_agent.get_next_path_position())
		
		if direction.length() > 1.0:
			velocity = direction.normalized() * delta * speed * 1000
			move_and_slide()
		elif _random_walk_timer.is_stopped():
			navigation_agent.target_position = global_position
	
	# Attack player
	if distance_to_player < distance_to_attack:
		if _attack_timer.is_stopped():
			_hurt_player()
			_attack_timer.start()
	else:
		_attack_timer.stop()

func _random_walk_timeout_func():
	_on_target_reached(true)

func _on_target_reached(is_timer: bool = false):
	if _pursuit:
		return
	
	if is_timer and _random_walk_timer.is_stopped():
		navigation_agent.target_position = global_position + Vector2(randf_range(-random_walk_radius, random_walk_radius), randf_range(-random_walk_radius, random_walk_radius))
	else:
		_random_walk_timer.start(random_walk_timeout + randf_range(-1.0, 1.0))

func _hurt_player():
	player.health.hurt(attack_value)
