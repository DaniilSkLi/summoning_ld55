extends Node2D

class_name Health

signal max_health_updated
signal health_updated

signal healed_all
signal healed
signal hurted
signal killed

@export var max_health: float = 10.0:
	set(new_value):
		if (new_value < 0):
			return
		max_health = new_value
		_health = min(_health, new_value)
		max_health_updated.emit(max_health)

@export_range(0, 1, 0.1) var initial_health_percent: float = 1.0



@export_group('Regeneration')

@export var regeneration_enabled: bool = false:
	set(new_value):
		regeneration_enabled = new_value
		if new_value:
			_regeneration_timer.start()
		else:
			_regeneration_timer.stop()

## Time to regeneration_health_pertime to health in seconds
@export var regeneration_time: float = 5.0:
	set(new_value):
		regeneration_time = new_value
		_regeneration_timer.wait_time = new_value

@export var regeneration_value_pertime: float = 1.0

@export var regeneration_delay_after_hurt: float = 10.0



var _health: float:
	set(new_value):
		if (new_value < 0):
			return
		_health = new_value
		health_updated.emit(new_value)

var _regeneration_timer: Timer = Timer.new()

func _ready():
	_health = max_health * initial_health_percent
	max_health_updated.emit(max_health)
	
	_regeneration_timer.autostart = false
	_regeneration_timer.wait_time = regeneration_time
	_regeneration_timer.timeout.connect(regeneration)
	add_child(_regeneration_timer)
	
	if regeneration_enabled:
		_regeneration_timer.start()



func regeneration():
	_health = min(_health + regeneration_value_pertime, max_health)
	_regeneration_timer.start(regeneration_time)

func heal_all():
	_health = max_health
	_heal_all()
	healed_all.emit()

func heal(value: float):
	_health = min(_health + value, max_health)
	_heal()
	healed.emit()

func hurt(value: float):
	if value <= 0:
		return
	
	_health = max(_health - value, 0)
	
	_regeneration_timer.start(regeneration_delay_after_hurt)
	
	if _health <= 0:
		kill()
	else:
		_hurt()
		hurted.emit()

func kill():
	_health = 0
	_kill()
	killed.emit()

func _heal_all():
	pass

func _heal():
	pass

func _hurt():
	pass

func _kill():
	pass
