extends Node2D

@export var target: Node2D

@onready var offset := position

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	global_position = target.global_position + offset
