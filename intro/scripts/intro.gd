extends Control

class_name Intro

@export var next_scene: PackedScene
@export_range(0, 5, 0.1) var delay: float = 1.0
@export_range(0, 5, 0.1) var audio_delay: float = 0.0
@export_range(0, 5, 0.1) var delay_between_splashes: float = 0.0
@export_range(0, 5, 0.1) var after_delay: float = 1.0
@export var skip_all_now: bool = false
@export var wait_audio: bool = false
@export var main_audio: AudioStreamOggVorbis
@export var splashes: Array[PackedScene]

@onready var _camera = $Camera2D
@onready var _tree = get_tree()

# Called when the node enters the scene tree for the first time.
func _ready():
	assert(next_scene)
	
	
	if not skip_all_now:
		await _tree.create_timer(audio_delay).timeout
			
		var player = AudioStreamPlayer.new()
		
		if main_audio:
			main_audio.loop = false
			add_child(player)
			
			player.stream = main_audio
			player.play()
		
		await _tree.create_timer(delay).timeout
		
		for splash in splashes:
			var scene = splash.instantiate(PackedScene.GEN_EDIT_STATE_DISABLED)
			add_child(scene)
			await scene.finished
			scene.queue_free()
			await _tree.create_timer(delay_between_splashes).timeout
		
		if wait_audio and player.playing:
			await player.finished
	
	await _tree.create_timer(after_delay).timeout
	_tree.change_scene_to_packed(next_scene)
	#_tree.root.add_child(next_scene.instantiate(PackedScene.GEN_EDIT_STATE_DISABLED))
	#_tree.unload_current_scene()
