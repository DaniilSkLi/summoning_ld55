extends Control

class_name SplashScreen

signal finished

@export_category("Durations")
@export var fadeIn: float = 1.5
@export var fadeOut: float = 2.0
@export var delay: float = 2.0

@export_category("Wait")

## The description of the variable.
@export var wait_list: Array[Node]

func _ready():
	var tween = create_tween()
	modulate.a = 0.0
	tween.tween_property(self, "modulate:a", 1.0, fadeIn)
	tween.tween_interval(delay)
	for item in wait_list:
		assert(item.finished)
		await item.finished
	tween.tween_property(self, "modulate:a", 0.0, fadeOut)
	if tween.is_running():
		await tween.finished
	
	finished.emit()
